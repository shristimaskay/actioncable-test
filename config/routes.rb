Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'

  resources :messages
  get "/room/:room_name" => "messages#room", as: :room

  root to: "pages#home"
end

