class MessagesController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    @message = Message.create(msg_params)
    if @message.save
      ActionCable.server.broadcast "channel_#{ @message.room_name }",
                                   content: "#{ @message.username } says: #{ @message.content } to room #{ @message.room_name}"
      head :ok
    end
  end

  def room
    @message = Message.new
  end
  private

  def msg_params
    params.require(:message).permit(:content, :username, :room_name)
  end
end
