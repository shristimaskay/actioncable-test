import consumer from "./consumer"
$(document).on('turbolinks:load', function () {
  consumer.subscriptions.create({
    channel: "RoomChannel",
    room_id: $('#room_messages').attr('data-channel-room') }, {
      
      connected() {
	console.log("connected to the room");
	// Called when the subscription is ready for use on the server
      },

      disconnected() {
	console.log("disconnected from the room");    
	// Called when the subscription has been terminated by the server
      },

      received(data) {
	console.log("Receiving data");
	console.log(data.content)
	$('#msg').append('<div class="message"> ' + data.content + '</div>');
	// Called when there's incoming data on the websocket for this channel
      }
    });


  let submit_messages;

  $(document).on('turbolinks:load', function () {
    submit_messages()
  })

  submit_messages = function () {
    $('#message_content').on('keydown', function (event) {
      if (event.keyCode == 13) {
	$('input').click()
	event.target.value = ''
	event.preventDefault()
      }
    })
  }
})
